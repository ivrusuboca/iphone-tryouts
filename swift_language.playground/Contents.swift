
import UIKit

// from : 
// https://developer.apple.com
// /library/ios/documentation/Swift/Conceptual/Swift_Programming_Language

/********************************************************************************
 *
 * The Basics
 *
 *******************************************************************************/


let maximumNumberOfLoginAttempts = 10;
var currentLoginAttempts = 0;

var x = 0.0, y = 1.1, z = 2.5;

var welcomeMessage:String;
welcomeMessage = "Hello there !";

var red, green, blue:Double

let π = 3.14159
let 你好 = "你好世界" // ( HelloThere = "Hello World" )
let 🐶🐮 = "dogcow"

print("Max number of attempts is : \(maximumNumberOfLoginAttempts)");

// this is a comment

/* this is
a multi-line comment */

/* this is a /* nested
multi-line */ comment ! */

let cat = "🐱"; print(cat)

//
// 8, 16, 32 & 64 signed and unsigned integers
//
// Int is Int32 on 32-bit platforms and Int64 on 64-bit platforms
// UInt is UInt32 on 32-bit platforms and UInt64 on 64-bit platforms
//
let minValue = UInt8.min  // minValue is equal to 0, and is of type UInt8
let maxValue = UInt8.max  // maxValue is equal to 255, and is of type UInt8

//
// Double represents a 64-bit floating-point number
// Float represents a 32-bit floating-point number
//

let meaningOfLife = 42	// inferred to be of type Int
let pi = 3.14159			// inferred to be of type Double

let decimalInteger = 17
let binaryInteger = 0b10001       // 17 in binary notation
let octalInteger = 0o21           // 17 in octal notation
let hexadecimalInteger = 0x11     // 17 in hexadecimal notation

let one = 1.25e2		// means 1.25 x 102, or 125.0.
let two = 1.25e-2		// means 1.25 x 10-2, or 0.0125.

// For hexadecimal numbers with an exponent of exp, the base number is multiplied by 2exp:
let three = 0xFp2		// means 15 x 2^2, or 60.0.
let four = 0xFp-2		// means 15 x 2^-2, or 3.75.

// extra-formatting helps with reading the number
let paddedDouble = 000123.456
let oneMillion = 1_000_000
let justOverOneMillion = 1_000_000.000_000_1

// integer conversion
let twoThousand: UInt16 = 2_000
let unit: UInt8 = 1
let twoThousandAndOne = twoThousand + UInt16(unit)

// type aliases
typealias AudioSample = UInt16

//
// Boolean constant values of type Bool : true and false
//

//
// tuples
//
let http404Error = (404, "Not Found")
let (statusCode, statusMessage) = http404Error
print("The status code is : \(statusCode)")
print("The status message is : \(statusMessage)")

let (justTheStatusCode, _) = http404Error
print("The status code is \(justTheStatusCode)")

print("The status code is \(http404Error.0)")
print("The status message is \(http404Error.1)")

// naming the fields inside a tuple so they can be used later
let http200Status = (statusCode: 200, description: "OK")

print("The status code is \(http200Status.statusCode)")
print("The status message is \(http200Status.description)")


//
// optionals
//

// convertedNumber is inferred to be of type "Int?", or "optional Int"
let possibleNumber = "123"
let convertedNumber = Int(possibleNumber)

var serverResponseCode: Int? = 404
serverResponseCode = nil	// nil cannot be used with non-optional consts and vars

var surveyAnswer: String?	// surveyAnswer is automatically set to nil

if convertedNumber != nil {
	print("convertedNumber has an integer value of \(convertedNumber!).")
}

//
// optional binding
//
if var actualNumber = Int(possibleNumber)
{
	print("\'\(possibleNumber)\' has an integer value of \(actualNumber)")
} else
{
	print("\'\(possibleNumber)\' could not be converted to an integer")
}

if let
	firstNumber = Int("4"),
	secondNumber = Int("42")
	where firstNumber < secondNumber
{
	print("\(firstNumber) < \(secondNumber)")
}

//
// implicitly unwrapped optionals
//

let possibleString: String? = "An optional string."
let forcedString: String = possibleString! // requires an exclamation mark

let assumedString: String! = "An implicitly unwrapped optional string."
let implicitString: String = assumedString // no need for an exclamation mark

// still can be treated a normal optional
if assumedString != nil
{
	print(assumedString)
}

//
// error handling
//

func canThrowAnError() throws {
	// this function may or may not throw an error
}

do{
	try canThrowAnError()
	// no error was thrown
} catch {
	// an error was thrown
}

//
// assertions
//

let age = -3
// assert( age >= 0, "A person's age cannot be less than zero")
// assert( age >= 0)

/********************************************************************************
*
* Classes and Structures
*
*******************************************************************************/

// classes
// - do not have a default memberwise initializer
// - are reference types : they are not copied but a reference to the existing instance is used
class SomeClass
{
	
}

// structures 
// - do not support inheritance
// - cannot type cast and interpret the type at runtime
// - do not have de-initializers
// - do not support reference counting
// - are value types : they are always copied when passed around in the code
struct SomeStructure
{
	var someValue = 0;
	var someOtherValue = 0.0;
}

// all structures have an automatically-generated memberwise initializer
let myStructure = SomeStructure( someValue: 5, someOtherValue: 8.9);

// identity operators
// ===	: identical to	: two constants or vars of a class type refer to exactly the same class instance
// !==	: not identical to

// example of structures 
// String
// Array
// Dictionary






































